#! /bin/bash
# https://github.com/codekoala/docker-pypi

sudo mkdir -p /srv/pypi             # local directory where packages reside
sudo touch /srv/pypi/.htpasswd      # credentials file for adding packages
docker run -t -i --rm -h pypi.local -v /srv/pypi:/srv/pypi:rw -p 1600:80 --name pypi  codekoala/pypi