Workflow
========

# Install and run Jenkins #

## Create persistent directory for Jenkins configs ##
mkdir /home/<user>/jenkinsarea

## Install ##
`./jenkins/build.sh`

## Just run Jenkins ##
`./start.sh`

## Jenkins URL ##
http://localhost:8080/


# PyPi #

## Build and run ##

`./pypi/build.sh`

## PyPi URL ##
http://localhost:1600/

## PyPi desc ##
To use this server with pip, run the the following command:

`pip install --extra-index-url http://localhost:1600/ PACKAGE [PACKAGE2...]`
To use this server with easy_install, run the the following command:

`easy_install -i http://localhost:1600/simple/ PACKAGE`
